import os
import uuid
import pfycat
import traceback
from helper import PicnicException

from pystreamable import StreamableApi


class NoNSFWException(PicnicException):
    pass


class NoUploadException(PicnicException):
    pass


class VidUpload(object):

    def __init__(self, user_agent, debug, dryrun):
        import secret

        self.dryrun = dryrun
        self.debug = debug

        self.client_streamable = StreamableApi(secret.streamable_user, secret.streamable_pass)

        self.pfycatclient = pfycat.Client()

        self.upload_list = [
            # self.upload_file_streamable, #broken
            self.upload_file_gfycat,
            # self.upload_file_insxnity, #broken
            # self.upload_file_openload, #hated by all
        ]

    def __call__(self, file_name, over_18):
        return self.upload_file(file_name, over_18)

    def upload_file_gfycat(self, locale_file_name, over_18):
        r = self.pfycatclient.upload(locale_file_name)
        return "https://gfycat.com/" + r["gfyname"]

    def upload_file_streamable(self, locale_file_name, over_18):
        if over_18:
            raise NoNSFWException()
        result = self.client_streamable.upload_video(locale_file_name, 'stable video')
        return 'https://streamable.com/' + result['shortcode']

    def upload_file(self, locale_file_name, over_18):
        # need unique filename for openload
        oldext = os.path.splitext(locale_file_name)[1]
        newName = str(uuid.uuid4()) + oldext
        os.rename(locale_file_name, newName)

        if self.dryrun:
            return "https://streamable.com/swt6z"

        for f in self.upload_list:
            try:
                return f(newName, over_18)
            except NoNSFWException:
                pass
            except Exception as e:
                print "Exception:"
                print e.__class__, e.__doc__, e.message
                print e
                traceback.print_exc()

        raise NoUploadException("could not upload stabilized file")
